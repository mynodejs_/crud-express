const express = require('express');
const bodyParser = require('body-parser');

// INICIALIZANDO A APLICACAO
const app = express();

// FORMATO DE PAYLOADS DE ENTRADA SERA EM JSON
app.use(bodyParser.json());
//MANIPULACAO DOS PARAMETROS DA URL
app.use(bodyParser.urlencoded({ extended: false}))

require('../src/app/controllers/index')(app);

app.listen(3000);
