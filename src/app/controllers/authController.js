const express = require('express');
const user = require('../models/user');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const authConfig = require('../../config/auth.json');
const mailer = require('../../modules/mailer');

const router = express.Router();

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

router.post('/register', async (req, res) => {
  const { email } = req.body;

  try {

    if (await user.findOne({ email })) {
      return res.status(400).send({error: 'User already exists'})
    }

    const newUser = await user.create(req.body);

    // RETIRANDO DO PAYLOAD DE RESPOSTA O ATRIBUTO SENHA
    newUser.password = undefined;

    return res.send({
      newUser,
      token: generateToken({ id: newUser.id}),
    });
  } catch (error) {
    return res.status(400).send({ error: 'Registration failed' });
  }
});


// ROTA DE AUTENTICACAO
router.post('/authenticate', async (req, res) => {
  const {email, password} = req.body;

  const registeredUser = await user.findOne({email}).select('+password');

  if (!registeredUser) {
    return res.status(400).send({ error: 'User not found' });
  }

  if (!await bcrypt.compare(password, registeredUser.password)) {
    return res.status(400).send({error: 'Invalid password'});
  }

  registeredUser.password = undefined;

  res.send({
    registeredUser,
    token: generateToken({ id: registeredUser.id })
  });
});


// RECUPERACAO DE SENHA
router.post('/forgot_password', async (req, res) => {
  const { email } = req.body;

  try {
    const forgotUser = await user.findOne({ email });

      if (!forgotUser) {
        return res.status(400).send({ error: 'User not found' });
      }

    //GERACAO DE TOKEN E DEFINICAO DA VALIDADE
    const token = crypto.randomBytes(20).toString('hex');
    const now = new Date();
    now.setHours(now.getHours() + 1);

    await user.findByIdAndUpdate(user.id, {
      '$set' : {
        passwordResetToken: token,
        passwordResetExpires: now,
      }
    });

    mailer.sendMail({
      to: email,
      from: 'vitoria@rocketseat.com.br',
      template: 'auth/forgot_password',
      context: { token },
    }, (err) => {
        return res.status(400).send({ error: 'Cannot send forgot password email'});

        return res.send();
    });
  } catch (err) {
      return res.status(400).send({error: 'Error on forgot password, try again'});
  }
});


router.post('/reset_password', async (req, res) => {
  const { email, token, password } = req.body;

  try {
    const resetUser = await user.findOne({ email })
          .select('+passwordResetToken passwordResetExpires');

    if (!resetUser) {
      return res.status(400).send({ error: 'User not found' });
    }

    if (token !== resetUser.passwordResetToken) {
      return res.status(400).send({ error: 'Invalid Token'});
    }

    const now = new Date();

    if (now > resetUser.passwordResetExpires) {
      return res.status(400).send({ error: 'Expires token, cannot reset password, try again'});
    }

    resetUser.password = password;
    await resetUser.save();

    res.send();
  } catch (error) {
    return res.status(400).send({ error: 'Cannot reser password, try again'});
  }
});

// TODA REQUISICAO DE REGISTRO SERA PRIMEIRAMENTE ACESSADA PELA ROTA /auth
module.exports = app => app.use('/auth', router);
