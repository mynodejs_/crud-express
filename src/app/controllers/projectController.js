  const express = require('express');
  const authMiddleware = require('../middlewares/auth');
  const project = require('../models/project');
  const task = require('../models/task');

  const router = express.Router();

  // ASSOCIANDO O MIDDLEWARE A ROTA
  // COM ISSO, A REQUISICAO NAO CHEGARA DIRETAMENTE AO CONTROLLER
  // SEM PASSAR PELAS VALIDACOES DO TOKEN DEFINIDAS NO MIDDLEWARE
  router.use(authMiddleware);

  router.get('/', async (req, res) => {
    try {

      // USO DE EAGGER LOADING PARA RECUPERACAO DOS DADOS
      // DO USUARIO ASSOCIADO AOS PROJETOS.
      // ISSO REDUZ A QUANTIDADE DE QUERYS EXECUTADAS.
      const projects = await project.find().populate(['user', 'tasks']);

      return res.send({ projects });
    } catch (e) {
      return res.status(400).send({ error: 'Error on recovered projects'})
    }
  });

  router.get('/:projectId', async (req, res) => {
    try {

      const projectDetail = await project.findById(req.params.projectId).populate(['user', 'tasks']);

      return res.send({ projectDetail });
    } catch (e) {
      return res.status(400).send({ error: 'Error on recovered project'})
    }
  });

  router.post('/create', async (req, res) => {
    try {
      const { title, description, tasks } = req.body;

      const newProject = await project.create({ title, description, user: req.userId });

      // ASSOCIANDO AS TASKS AO PROJETO
    await Promise.all(tasks.map(async t => {
        const projectTask = new task({ ...task, project: newProject._id });

        await projectTask.save();

        newProject.tasks.push(projectTask );
      }));

      await newProject.save();

      return res.send({ newProject });
    } catch (e) {
      console.log(e);
      return res.status(400).send({ error: 'Error creating new project' })
    }
  });


  router.put('/:projectId', async (req, res) => {
    try {
      const { title, description, tasks } = req.body;

      const newProject = await project.findByIdAndUpdate(req.params.projectId, {
        title,
        description
      }, {new: true}); // RETORNA O PROJETO COM INFOS ATUALIZADAS

      newProject.tasks = [];
      await task.remove({ project: newProject._id });

      await Promise.all(tasks.map(async t => {
        const projectTask = new task({ ...task, project: newProject._id });

        await projectTask.save();

        newProject.tasks.push(projectTask);
      }));

      await newProject.save();

      return res.send({ newProject });
    } catch (e) {
      console.log(e);
      return res.status(400).send({ error: 'Error updating project' })
    }
  });


  router.delete('/:projectId', async (req, res) => {
    try {

     await project.findByIdAndRemove(req.params.projectId);

      return res.send();
    } catch (e) {
      return res.status(400).send({ error: 'Error on recovered project'})
    }
  });



  module.exports = app => app.use('/projects', router);
