const mongoose = require('../../database');
const bcrypt = require('bcryptjs');

// CRIACAO DA TABELA DE USUARIO E DEFINICOES/REGRAS DOS CAMPOS
const userSchema = new mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
  },

  password: {
    type: String,
    required: true,
    select: false //IMPEDE DA SENHA APARECER NUMA CONSULTA,
  },

  passwordResetToken: {
    type: String,
    select: false,
  },

  passwordResetExpires: {
    type: Date,
    select: false,
  },

  createdAt: {
    type: Date,
    default: Date.now,
  },
});

userSchema.pre('save', async function(next){
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;

  next();
});

const user = mongoose.model('user', userSchema);

module.exports = user;
