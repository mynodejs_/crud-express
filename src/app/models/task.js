const mongoose = require('../../database');
const bcrypt = require('bcryptjs');

const taskSchema = new mongoose.Schema({
  title: {
    type: String,
    require: true,
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'project',
    require: true,
  },
  assignedTo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    require: true,
  },
  completed: {
    type: Boolean,
    require: true,
    default: false,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    require: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});


const task = mongoose.model('task', taskSchema);

module.exports = task;
